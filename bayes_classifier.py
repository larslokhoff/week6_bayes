## Name: Robin Hansma (10624368)
## Name: Lars Lokhoff (10606165)
## Name: Daan van Ingen (10345078)

from __future__ import division
from collections import Counter,defaultdict
from bs4 import BeautifulSoup
from nltk.corpus import stopwords
import os
import glob
import nltk
import math
import pickle
from operator import itemgetter
import numpy as np
import re
import csv
import unicodedata
import string

force = True # Variable for rebuilding index

def parse_xml(folder):
	print 'parse_xml'
	index = {}
	usedfiles = []
	for infile in glob.glob( os.path.join(folder, '*.xml')):
		with open(infile, 'r') as f:
			soup = BeautifulSoup(f.read(), "lxml")
			ministerie = soup.findAll("item", {"attribuut": "Afkomstig_van"})
			if ministerie:
				usedfiles.append(infile)
				ministerie = str(ministerie[0]).replace('<item attribuut="Afkomstig_van">', '').replace('</item>', '').lower()
				ministerie = re.sub('\([^)]*\)', '', ministerie)
				ministerie = re.sub('\s\s+', ' ', ministerie)
				ministerie = unicode(ministerie, "utf-8")
				ministerie = ''.join((c for c in unicodedata.normalize('NFD', ministerie) if unicodedata.category(c) != 'Mn'))
				ministerie = unicodedata.normalize('NFKD', ministerie).encode('ascii','ignore')
				ministerie = ministerie.strip()
				vragen = soup.findAll("vraag")
				for vraag in vragen:
					if(ministerie not in index):
						index[ministerie] = []
					# We assume there are no duplicate questions.
					vraag = re.sub('\<.*?\>', '', str(vraag))
					vraag = re.sub('\s\s+', ' ', str(vraag))
					vraag = unicode(vraag, "utf-8")
					vraag = ''.join((c for c in unicodedata.normalize('NFD', vraag) if unicodedata.category(c) != 'Mn'))
					vraag = unicodedata.normalize('NFKD', unicode(vraag)).encode('ascii','ignore')
					vraag = normalize(vraag)
					index[ministerie].append(vraag)
	return index, usedfiles

def remove_stop_words(vraag):
	stopwords_dutch = stopwords.words('dutch')

	for s in stopwords_dutch:
		vraag = vraag.replace(' ' + s + ' ', ' ')

	return vraag

def remove_punctuation(text):
	for p in string.punctuation:
		text = text.replace(p, '')

	return text

def normalize(text):
	text = remove_punctuation(text)
	text = remove_stop_words(text)

	return text.lower()

def writeToCSV(dict, csvfile):
	with open(csvfile, 'w') as f:
		c = csv.writer(f)
		for key, value in dict.items():
			c.writerow([key] + value)

def save_obj(obj, name):
	with open('obj/'+ name + '.pkl', 'wb') as f:
		pickle.dump(obj, f)

def load_obj(name):
	with open('obj/' + name + '.pkl', 'rb') as f:
		return pickle.load(f)

def extract_vocabulary(index, ministeries_used):
	vocabulary = []
	N = 0

	for ministerie in ministeries_used:
		vragen = index[ministerie[0]]
		N += len(vragen)
		for vraag in vragen:
			vraag = vraag.split(' ')
			for token in vraag:
				vocabulary.append(token)

	return vocabulary, N

def count_tokens_of_term(documents, term):
	tf = 0
	for document in documents:
		document = document.split(' ')
		for token in document:
			token = token

			if token == term:
				tf += 1

	return tf

def create_inverted_index(index, ministeries_used):
	if (not os.path.isfile('obj/inverted_index.pkl')) or force:
		tokens = {}
		for ministerie in ministeries_used:
			vragen = index[ministerie[0]]
			for vraag in vragen:
				vraag = vraag.split(' ')
				for token in vraag:
					token = token
					if token not in tokens:
						tokens[token] = {}

					if ministerie[0] not in tokens[token]:
						tokens[token][ministerie[0]] = 0

					tokens[token][ministerie[0]] += 1

		save_obj(tokens, 'inverted_index')

	tokens = load_obj('inverted_index')

	return tokens


def train_multinomial_nb(index, ministeries_used):
	V, N = extract_vocabulary(index, ministeries_used) # Complete vocabulary
	prior = {}

	inverted_index = create_inverted_index(index, ministeries_used)
	cond_prob = {}

	for ministerie in ministeries_used:
		Nc = len(index[ministerie[0]])
		prior[ministerie[0]] = Nc/N
		Tct = {}
		Tct['total'] = 0
		for t in V:
			if ministerie[0] not in inverted_index[t]:
				inverted_index[t][ministerie[0]] = 0
			Tct[t] = inverted_index[t][ministerie[0]] #count_tokens_of_term(index[ministerie[0]], t)
			Tct['total'] += Tct[t]

		for t in V:
			if t not in cond_prob:
				cond_prob[t] = {}

			cond_prob[t][ministerie[0]] = (Tct[t] + 1) / ((Tct['total'] - Tct[t]) + (len(Tct) - 1))

	return V, prior, cond_prob


def apply_multinomail_nb(classes, vocabulary, prior, condprob, document):
	document = remove_punctuation(document)
	tokens = document.split(' ')
	score = {}
	for classname in classes:
		classname = classname[0]
		if classname not in score:
			score[classname] = 0

		score[classname] = np.log(prior[classname])
		for token in tokens:
			token = token.lower()
			token = ''.join((c for c in unicodedata.normalize('NFD', unicode(token)) if unicodedata.category(c) != 'Mn'))
			token = unicodedata.normalize('NFKD', unicode(token)).encode('ascii','ignore')
			if token not in cond_prob:
				score[classname] += 0
			else:
				if classname in condprob[token]:
					score[classname] += np.log(condprob[token][classname])

	scores_in_array = []
	for classname in score:
		scores_in_array.append([classname, score[classname]])

	sorted_scores = sorted(scores_in_array, key=itemgetter(1))
	return sorted_scores[-1]

def get_top_10(index):
	numkeys = len(index)
	numquest = np.empty((numkeys, 2), dtype = object)

	count = 0
	totalquestions = 0
	for keys, values in index.items():
		numquest[count][0] = keys
		numquest[count][1] = len(values)
		totalquestions += len(values)
		count += 1

	numquestsorted = numquest[np.argsort(numquest[:, 1])]
	ministeries_used = numquestsorted[-10:]

	return ministeries_used

def test_classifier(classes, vocabulary, prior, condprob):
	test_questions, usedfiles = parse_xml('KVR_TEST')
	results_per_class = {}
	total_results = {}
	total_results['goed'] = 0
	total_results['fout'] = 0

	for classname in classes:
		ministerie = classname[0]
		results_per_class[ministerie] = {}
		results_per_class[ministerie]['goed'] = 0
		results_per_class[ministerie]['fout'] = 0
		for question in test_questions[ministerie]:
			result = apply_multinomail_nb(classes, vocabulary, prior, condprob, question)
			if result[0] == ministerie:
				results_per_class[ministerie]['goed'] += 1

			else:
				results_per_class[ministerie]['fout'] += 1

		total_results['goed'] += results_per_class[ministerie]['goed']
		total_results['fout'] += results_per_class[ministerie]['fout']

	print "Total right: ", total_results['goed']
	print "Total wrong: ", total_results['fout']
	print "Percentage right: ", (total_results['goed']/(total_results['fout']+total_results['goed'])) * 100

def get_precision_and_recall(classes, vocabulary, prior, condprob):
	test_questions, usedfiles = parse_xml('KVR_TEST')

	results_per_class = {}

	for classname in classes:
		ministerie = classname[0]
		results_per_class[ministerie] = {}
		results_per_class[ministerie]['goed'] = 0
		for question in test_questions[ministerie]:
			result = apply_multinomail_nb(classes, vocabulary, prior, condprob, question)

			if result[0] == ministerie:
				results_per_class[ministerie]['goed'] += 1

			else:
				if result[0] not in results_per_class[ministerie]:
					results_per_class[ministerie][result[0]] = 0

				results_per_class[ministerie][result[0]] += 1

	positives_negatives_per_class = {}
	for ministerie in results_per_class:
		positives_negatives_per_class[ministerie] = {}
		positives_negatives_per_class[ministerie]['TP'] = results_per_class[ministerie]['goed']
		positives_negatives_per_class[ministerie]['TN'] = 0
		positives_negatives_per_class[ministerie]['FP'] = 0
		positives_negatives_per_class[ministerie]['FN'] = 0

	for ministerie in results_per_class:
		for otherclass in results_per_class[ministerie]:
			if otherclass != 'goed':
				positives_negatives_per_class[ministerie]['FP'] += results_per_class[ministerie][otherclass]

				positives_negatives_per_class[otherclass]['FN'] += results_per_class[ministerie][otherclass]

				for otherclass_2 in results_per_class[ministerie]:
					if otherclass != otherclass_2:
						# print results_per_class[ministerie][otherclass_2]
						positives_negatives_per_class[otherclass]['TN'] += results_per_class[ministerie][otherclass_2]

		print ministerie, positives_negatives_per_class[ministerie]

def calcN01(inverted_index_word, key):
	total = 0
	for ministerie, value in inverted_index_word.items():
		if(ministerie == key):
			continue
		else:
			total += value
	return total

def calcN10(inverted_index, key, not_word):
	total = 0
	for word, value in inverted_index.items():
		if(word != not_word):
			for ministerie, score in value.items():
				if(ministerie == key):
					total += score
	return total

def calcN00(inverted_index, key, not_word):
	total = 0
	for word, value in inverted_index.items():
		if(word != not_word):
			for ministerie, score in value.items():
				if(ministerie != key):
					total += score
	return total

# Doesnt work, or maybe it does but it takes a long time to compute
def top_10_per_class(index, ministeries_used):
	inverted_index = create_inverted_index(index, ministeries_used)
	# path, dirs, files = os.walk('KVR_TRAIN').next()
	# total_files = len(files)
	num_keys = len(inverted_index.keys())
	print num_keys
	count = 0
	top_10_per_class_word = {}
	top_10_per_class_score = {}
	# once = True
	for word, value in inverted_index.items():
		count += 1
		print count/num_keys
		for key, score in value.items():
			N11 = inverted_index[word][key]
			N01 = calcN01(inverted_index[word], key)
			N10 = calcN10(inverted_index, key, word)
			# N00 = calcN00(inverted_index, key, word)
			Ntotal = 380176 # Standard
			N00 = 380176 - N11 - N01 - N10
			# print N11, N01, N10, N00, Ntotal
			I1, I2, I3, I4 = 0, 0, 0, 0
			if(N11 != 0):
				I1 = (N11/Ntotal)*math.log((Ntotal*N11)/((N11+N10)*(N11+N01)))
			if(N01 != 0):
				I2 = (N01/Ntotal)*math.log((Ntotal*N01)/((N01+N00)*(N11+N01)))
			if(N10 != 0):
				I3 = (N10/Ntotal)*math.log((Ntotal*N10)/((N11+N10)*(N10+N00)))
			if(N00 != 0):
				I4 = (N00/Ntotal)*math.log((Ntotal*N00)/((N01+N00)*(N10+N00)))
			I = I1 + I2 + I3 + I4
			if key not in top_10_per_class_word: # First occ of ministerie
				top_10_per_class_word[key] = []
				top_10_per_class_score[key] = []
				top_10_per_class_word[key].insert(0, word)
				top_10_per_class_score[key].insert(0, I)
			else:
				for i in range(len(top_10_per_class_word[key])):
					if(i > 9):
						continue
					if(I > top_10_per_class_score[key][i]):
						top_10_per_class_word[key].insert(i, word)
						top_10_per_class_score[key].insert(i, score)
						break

	# print top_10_per_class_word
	# print top_10_per_class_score
	for key, value in top_10_per_class_word.items():
		print key, top_10_per_class_word[key][:10]
		# print top_10_per_class_score[key][:10]

	save_obj(top_10_per_class_word, 'top_10_word')
	save_obj(top_10_per_class_score, 'top_10_score')
	print top_10_per_class_word

if __name__ == '__main__':
	if((not os.path.isfile('obj/Qdict.pkl')) or force):
		Questions_dict, usedfiles = parse_xml('KVR_TRAIN')
		save_obj(Questions_dict, 'Qdict')
		np.save('obj/filesused', usedfiles)

	filesused = np.load('obj/filesused.npy')
	index = load_obj('Qdict')

	ministeries_used = get_top_10(index)
	V, prior, cond_prob = train_multinomial_nb(index, ministeries_used)

	get_precision_and_recall(ministeries_used, V, prior, cond_prob)

	top_10_per_class(index, ministeries_used)
	#test_classifier(ministeries_used, V, prior, cond_prob)
