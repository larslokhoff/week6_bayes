## Name: Robin Hansma (10624368)
## Name: Lars Lokhoff (10606165)
## Name: Daan van Ingen (10345078)

Text classification exercise
Naive Bayes

File explanation
folders and files:
KVR -> stores kamervragen xml files
KVR_TEST -> stores the test kamervragen xml files
KVR_TRAIN -> stores the training data xml files
obj -> contains some dict objects save using pickle to make bayes_classifier.py run faster

program:
bayes_classifier.py

parses the training set xml files to create an dict index with the used questions per class.
Then it uses the top 10 ministeries to continue the program. We chose the top 10
to have clear class names and have sufficient data.
Then the prior and conditional chances are calculated.

Then we tried finding the top 10 per class making use of mutual information.
Unfortunately the implemented algorithm was too slow. To calculate the number of
documents (questions) that had class A and not word B takes too long with the
indexes we created. The function is present but the function call is commented out.

Then we calculate the precision and recall per class and test the created
classifier.
